package com.fastnews.service.api

import android.content.Context
import android.util.Log
import com.fastnews.mechanism.VerifyNetworkInfo
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

abstract class RedditAPI {
    companion object {

        private lateinit var redditApiService: RedditService
        private var mCache: Cache? = null

        private const val API_BASE_URL: String = "https://www.reddit.com/r/Android/"

        fun getService(context: Context): RedditService {

            if (!Companion::redditApiService.isInitialized) {
                redditApiService = getCachedRetrofit(context)?.create<RedditService>(
                    RedditService::class.java
                )!!
            }

            return redditApiService

        }

        private fun getCachedRetrofit(context: Context): Retrofit? {
            val httpClient =
                OkHttpClient.Builder() // Add all interceptors you want (headers, URL, logging)
                    .addInterceptor(httpLoggingInterceptor())
                    .addInterceptor(provideOfflineCacheInterceptor())
                    .addNetworkInterceptor(provideCacheInterceptor(context))
                    .cache(provideCache(context))

            return Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(httpClient.build())
                .build()
        }

        private fun httpLoggingInterceptor() : HttpLoggingInterceptor {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            return loggingInterceptor
        }

        private fun provideCache(context: Context): Cache? {
            if (mCache == null) {
                try {
                    if (context != null) {
                        mCache = Cache(
                            File(context.cacheDir, "http-cache"),
                            10 * 1024 * 1024
                        )
                    } // 10 MB
                } catch (e: Exception) {
                    Log.e("", "Could not create Cache!")
                }
            }
            return mCache
        }

        private fun provideCacheInterceptor(context: Context): Interceptor? {
            return Interceptor { chain: Interceptor.Chain ->
                val response = chain.proceed(chain.request())
                val cacheControl: CacheControl = if (VerifyNetworkInfo.isConnected(context)) {
                    CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS)
                        .build()
                } else {
                    CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()
                }
                response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()
            }
        }

        private fun provideOfflineCacheInterceptor(): Interceptor? {
            return Interceptor { chain: Interceptor.Chain ->
                try {
                    chain.proceed(chain.request())
                } catch (e: java.lang.Exception) {
                    val cacheControl = CacheControl.Builder()
                        .onlyIfCached()
                        .maxStale(1, TimeUnit.DAYS)
                        .build()
                    val offlineRequest = chain.request().newBuilder()
                        .cacheControl(cacheControl)
                        .build()
                    chain.proceed(offlineRequest)
                }
            }
        }
    }
}
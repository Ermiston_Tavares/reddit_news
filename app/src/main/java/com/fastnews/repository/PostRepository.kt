package com.fastnews.repository

import android.content.Context
import com.fastnews.service.api.RedditAPI
import com.fastnews.service.model.PostDataChild

object PostRepository : BaseRepository() {

    suspend fun getPosts(after: String, limit: Int, context : Context): PostDataChild? {

        val postResponse = safeApiCall(
            call = { RedditAPI.getService(context).getPostList(after, limit).await() },
            errorMessage = "Error to fetching posts"
        )

        return postResponse?.data

    }
}
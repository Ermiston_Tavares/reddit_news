package com.fastnews.ui.timeline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.fastnews.R
import com.fastnews.mechanism.VerifyNetworkInfo
import com.fastnews.service.model.PostData
import com.fastnews.ui.detail.DetailFragment.Companion.KEY_POST
import com.fastnews.ui.timeline.listener.PaginationScrollListener
import com.fastnews.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.fragment_timeline.*


class TimelineFragment : Fragment() {

    private lateinit var layoutManager : LinearLayoutManager
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    private var next : String = ""
    private lateinit var mPostViewModel: PostViewModel

    private lateinit var adapter: TimelineAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_timeline, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPostViewModel = ViewModelProvider(this).get(PostViewModel::class.java)
        observe()
        buildActionBar()
        buildTimeline()
        clickVerifyConnectionState()
        verifyConnectionState()
    }

    private fun verifyConnectionState() {
        hideNoConnectionState()
        showProgress()
        fetchTimeline()
    }

    private fun clickVerifyConnectionState() {
        state_without_conn_timeline.setOnClickListener {
            verifyConnectionState()
        }
    }

    private fun buildActionBar() {
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(false) // disable the button
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false) // remove the left caret
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getString(R.string.app_name)
    }

    private fun buildTimeline() {
        adapter = TimelineAdapter { it, imageView ->
            onClickItem(it, imageView)
        }

        layoutManager = LinearLayoutManager(context)
        timeline_rv.layoutManager = layoutManager
        timeline_rv.itemAnimator = DefaultItemAnimator()
        timeline_rv.adapter = adapter

        //Pagination
        buildRecyclerViewOnScrollListener()
    }

    private fun buildRecyclerViewOnScrollListener() {
        timeline_rv?.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                adapter.addLoading()
                fetchTimeline(next)
            }
        })

    }

    private fun observe() {
        mPostViewModel.posts.observe(this, Observer {
            if (it != null) {
                removeLoading()
                this.next = it.after.toString()
                val result: ArrayList<PostData> = ArrayList()
                it.children?.map { postChildren -> result.add(postChildren.data) }
                adapter.setData(result)
                hideProgress()
                showPosts()
            } else {
                hideProgress()
                showNoConnectionState()
            }
        })
    }

    private fun removeLoading() {
        if (isLoading) {
            isLoading = false
            adapter.removeLoading()
        }
    }


    private fun fetchTimeline(next: String = "") {
        mPostViewModel.getPosts(next, 50)
    }

    private fun showPosts() {
        timeline_rv.visibility = View.VISIBLE
    }

    private fun showProgress() {
        state_progress_timeline.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        state_progress_timeline.visibility = View.GONE
    }

    private fun showNoConnectionState() {
        state_without_conn_timeline.visibility = View.VISIBLE
    }

    private fun hideNoConnectionState() {
        state_without_conn_timeline.visibility = View.GONE
    }

    private fun onClickItem(postData: PostData, imageView: ImageView) {
        val extras = FragmentNavigatorExtras(
            imageView to "thumbnail"
        )
        var bundle = Bundle()
        bundle.putParcelable(KEY_POST, postData)
        findNavController().navigate(R.id.action_timeline_to_detail, bundle, null, extras)
    }
}
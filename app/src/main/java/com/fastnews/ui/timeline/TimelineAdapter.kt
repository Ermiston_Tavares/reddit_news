package com.fastnews.ui.timeline

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.fastnews.R
import com.fastnews.service.model.PostData
import kotlinx.android.synthetic.main.include_item_timeline_thumbnail.view.*

class TimelineAdapter(val onClickItem: (PostData, ImageView) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoaderVisible = false
    var items: ArrayList<PostData> = ArrayList()

    companion object {
        private const val TYPE_LOADING = 0
        private const val TYPE_NORMAL = 1
    }

    fun setData(items: ArrayList<PostData>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == TYPE_LOADING) {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_loading,
                parent,
                false
            )
            ProgressHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_timeline,
                parent,
                false
            )

            TimelineItemViewHolder(view)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is TimelineItemViewHolder) {
            holder.data = items[position]
            holder.view.setOnClickListener { onClickItem(
                items[position],
                holder.view.item_timeline_thumbnail
            ) }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            if (position == items.size - 1) {
                TYPE_LOADING
            } else {
                TYPE_NORMAL
            }
        } else {
            TYPE_NORMAL
        }
    }

    fun addLoading() {
        isLoaderVisible = true
        this.items.add(items[0])
        notifyItemInserted(items.size - 1)
    }

    fun removeLoading() {
        isLoaderVisible = false
        val position: Int = items.size - 1

        items.removeAt(position)
        notifyItemRemoved(position)
    }
}